# Kotlinでも代数学

Kotlinでも代数拡大するデモ  
[ref. Kotlinでも代数学](https://blog.regrex.jp/2017/03/31/post-1822/)

PR歓迎です。

# ビルドおよび実行

Kotlinコンパイラ、もしくはIntelliJ IDEAとかEclipseとかのIDE。WebのTry Kotlinは非推奨。

## Gradleがインストールされている場合

プロジェクトのルートディレクトリ（`build.gradle`があるところ）に移動して

    gradle
    
を実行するとビルドされ，`main.kt`に書かれた処理が実行されます．

## Gradleがインストールされていない場合

プロジェクトのルートディレクトリに移動して，Linux，Macの場合は

    ./gradlew
    
Windowsなら

    gradlew.bat

を実行します．
初回はバイナリがダウンロードされ時間がかかりますがそのうちビルド・実行されます．
2回目以降はダウンロードされたバイナリが使用されるので時間はかかりません．

## IntelliJ IDEAを使う場合

プロジェクトのルートディレクトリを開き，メニューから「Run > Run...」を選択，表示されるダイアログで`MainKt`を選択するとビルド・実行されます．

# ToDo

* Polynomial*系のtoString()が悲しいことになっているので修正する
* パッケージとかちゃんとする
* テストを書く
